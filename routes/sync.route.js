const { User, Book, UserSession } = require('../models/models');
const router = require('express').Router();

router.post('/user', async (req, res) => {
    try {
        await User.sync({ force: true });
        return res.status(201).json('CREATED USER TABLE');
    } catch (e) {
        return res.status(500).json(e);
    }
});

router.post('/user-session', async (req, res) => {
    try {
        await UserSession.sync({ force: true });
        return res.status(201).json('CREATED USER SESSION TABLE');
    } catch (e) {
        return res.status(500).json(e);
    }
});

router.post('/book', async (req, res) => {

    try {
        await Book.sync({ force: true });
        return res.status(201).json('CREATED BOOK TABLE');
    } catch (e) {
        return res.status(500).json(e);
    }

});


module.exports = router;