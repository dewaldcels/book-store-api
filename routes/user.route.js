const { User, UserSession } = require('../models/models');
const router = require('express').Router();
const Response = require('../models/response.model');
const TokenTool = require('../models/token.model');
const moment = require('moment');
const LOGIN_DATE_FORMAT = 'YYYY-MM-DD HH:mm';

router.get('/', async (req, res) => {
    const r = new Response();
    try {
        r.data = await User.findAll();
    }
    catch (e) {
        r.setError(e);
    }

    return res.status(r.status).json(r);
});

router.get('/:user_id', async (req, res) => {

    const r = new Response();
    const { user_id } = req.params;

    try {
        r.data = await User.findByPk(user_id);
        if (r.data == null) {
            throw({ status: 404, message: 'User not found'});
        }
    } catch (e) {
        r.setError(e);
    }

    return res.status(r.status).json(r);

});

router.post('/login', async (req, res) => {

    const { user } = req.body;
    const r = new Response();

    try {

        if (!user) {
            throw ({ status: 400, message: 'No user data was received in the request.' });
        }
        if (!user.email || !user.password) {
            throw ({ status: 400, message: 'Please provide a email and password to login.' });
        }

        const loginUser = await User.findOne({
            attributes: ['id', 'full_name', 'email', 'password', 'last_login', 'created_at', 'updated_at'],
            where: {
                email: user.email
            }
        });

        if (loginUser == null || loginUser.active == 0) {
            throw ({ status: 401, message: `The user ${user.email} does not exist.` });
        }

        if (!User.comparePassword(user.password, loginUser.password)) {
            throw ({ status: 401, message: `You have provided incorrect login credentials for ${user.email}.` });
        }

        const existingSession = await UserSession.findOne({
            where: {
                user_id: loginUser.id,
                active: 1
            }
        });

        let token;

        if (existingSession == null) {
            token = TokenTool.sign({ user_id: loginUser.id });
            await UserSession.create({
                user_id: loginUser.id,
                token: token,
            });
        } else {
            token = existingSession.token;
        }

        const lastLogin = moment().format(LOGIN_DATE_FORMAT);

        await User.update({
            last_login: lastLogin
        }, {
            where: {
                id: loginUser.id
            }
        });

        r.data = {
            user: {
                full_name: loginUser.full_name,
                email: loginUser.email,
                last_login: lastLogin,
                created_at: loginUser.created_at,
                updated_at: loginUser.updated_at
            },
            token: token
        };

    } catch (e) {
        r.setError(e);
    }

    return res.status(r.status).json(r);

});

router.post('/register', async (req, res) => {

    const { user } = req.body;
    const r = new Response();

    try {

        if (!user) {
            throw ({ status: 400, message: 'No user data was received in the request.' });
        }

        if (!user.full_name) {
            throw ({ status: 400, message: 'The full_name property is required.' });
        }

        if (!user.email) {
            throw ({ status: 400, message: 'The email address is required.' });
        }

        if (!user.password){
            throw ({ status: 400, message: 'Your password can not be empty.' });
        }

        const hasUser = await User.findAll({
            where: {
                email: user.email
            }
        });

        if (hasUser.length > 0) {
            throw ({ status: 400, message: `The email ${user.email} is already registered.` });
        }

        const plainPassword = user.password;
        user.password = User.generatePassword(plainPassword);
        user.last_login =  moment().format(LOGIN_DATE_FORMAT);
        const newUser = await User.create(user);
        const token = TokenTool.sign({ user_id: newUser.id });

        const userSession = await UserSession.create({
            user_id: newUser.id,
            token: token
        });

        r.data = {
            user: {
                full_name: newUser.full_name,
                email: newUser.email,
                last_login: newUser.last_login,
                created_at: newUser.created_at,
                updated_at: newUser.updated_at
            },
            token
        };

        r.status = 201;
    } catch (e) {
        r.setError(e);
    }
    return res.status(r.status).json(r);

});

router.patch('/logout', async (req, res) => {

    const r = new Response();
    const { user_id } = req;

    try {
        await UserSession.update({
            active: 0
        }, {
            where: {
                user_id: user_id
            }
        });
        r.data = {
            result: 'success'
        }
    } catch (e) {
        r.setError(e);
    }

    return res.status(r.status).json(r);

});

router.patch('/', async (req, res)=>{

    const r = new Response();
    const { user } = req.body;

    try {

        if (!user) {
            throw ({status: 400, message: "No user information was provided. "});
        }

        if (!user.hasOwnProperty('full_name')) {
            throw ({status: 400, message: "Could not find the users full name."});
        }

        await User.update({
            full_name: user.full_name
        }, {
            where: {
                id: req.user_id
            }
        });

        r.data = await User.findByPk(req.user_id, {
            attributes: ['full_name', 'email','last_login','created_at','updated_at']
        });

    } catch (e){ 
        r.setError(e);
    }

    return res.status(r.status).json(r);
})

module.exports = router;