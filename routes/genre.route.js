const { Genre, UserSession } = require('../models/models');
const router = require('express').Router();
const Response = require('../models/response.model');
const TokenTool = require('../models/token.model');
const moment = require('moment');

router.get('/', async (req, res) => {
    
    const r = new Response();

    try {
        r.data = await Genre.findAll({
            where: {
                active: 1
            }
        });
    }
    catch (e) {
        r.setError(e);
    }

    return res.status(r.status).json(r);
});

router.post('/', async (req, res) => {

    const { genre } = req.body;
    const r = new Response();

    try {

        if (!genre) {
            throw ({ status: 400, message: 'No genre data was received in the request.' });
        }
        if (!genre.name) {
            throw ({ status: 400, message: 'Please provide a name for the new genre.' });
        }

        r.data = await Genre.create(genre);

    } catch (e) {
        r.setError(e);
    }

    return res.status(r.status).json(r);

});

module.exports = router;