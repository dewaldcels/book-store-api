const { Model, DataTypes } = require('sequelize');
const { db } = require('../config/db.config');

class Genre extends Model {}

Genre.init({
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: DataTypes.STRING,
    active: {
        type: DataTypes.SMALLINT,
        defaultValue: 1
    }
}, {
    sequelize: db,
    modelName: 'genre'
});

module.exports = Genre;