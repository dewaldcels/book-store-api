const User = require('./user.model');
const Book = require('./book.model');
const UserSession = require('./user-session.model');

// User.belongsTo(UserSession, {
//     foreignKey: 'id',
//     targetKey: 'user_id'
// });

module.exports = {
    User,
    Book,
    UserSession
};