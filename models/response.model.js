class Response {

    constructor() {
        this.data = [];
        this.status = 200;
        this.error = null;
    }

    setError(e) {

        let { status = 500 } = e;
        let { message = e } = e;

        if (e.errors && e.errors.length > 0 && e.errors[0].message) {
            message = e.errors[0].message;
            status = 400;
        }

        this.status = status;
        this.error = message;
    }
}

module.exports = Response;