# Book Store API

Book Store API is built using Node.JS with Express.

## Required fields on .env file:
```
DB_DATABASE
DB_USER
DB_PASS
DB_HOST
DB_PORT
DB_DIALECT
HASH_SECRET_PASSWORD
HASH_SECRET_TOKEN
HASH_ROUNDS
```

## Install
Before running the application you will need to execute `npm install` to install all the required dependencies. 

## Run
Running the project is done by executing `npm start`.

## Endpoints
See documentation for endpoints.