const { Sequelize } = require('sequelize');

module.exports.db = new Sequelize(process.env.DB_DATABASE, process.env.DB_USER, process.env.DB_PASS, {
    host: process.env.DB_HOST,
    dialect: process.env.DB_DIALECT,
    define: {
        freezeTableName: true,
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        underscored: true
    }
});

